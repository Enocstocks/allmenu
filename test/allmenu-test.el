(load-file "../allmenu.el")

;; This test needs improvement in generate a good enviroment to run across different
;; emacs customizations or modify allmenu-buffer-list to be friendlier to test.
;; To make this test work you should only have this buffer open, you can also have
;; special buffers (those whose name is between *) open since the vanilla configuration
;; of allmenu avoid those buffers

;; These tests open files of different programming languages, which are in the same directory
;; as the tests, and test if allmenu works as it should. The programming languages of the files
;; were chosen by its mode, since different modes use different methods to create an index
;; alist

(defun test-enviroment (callback)
  "Generic procedures to set and recover previous emacs state before running
a test."
  (unwind-protect
      (progn
        (find-file-noselect "hello-world.el")
        (find-file-noselect "hello-world.c")
        (find-file-noselect "hello-world.rb")
        (funcall callback))
    (kill-buffer "hello-world.el")
    (kill-buffer "hello-world.c")
    (kill-buffer "hello-world.rb")))

(defun buffer-list-callback ()
  "Test `allmenu--buffer-list'. It should return only qualified buffers"
  (let ((allmenu-buffers allmenu-searched-buffers))
    (unwind-protect
        (progn
          (with-current-buffer (current-buffer)
          (should (equal (allmenu--buffers-list)
                         '("allmenu-test.el" "hello-world.el" "hello-world.c" "hello-world.rb"))))
      (setq allmenu-searched-buffers allmenu-buffers)))))

(ert-deftest test-allmenu--buffer-list ()
  (test-enviroment 'buffer-list-callback))

(ert-deftest test-allmenu-merge-alist ()
  "Test allmenu-merge-alist. It should merge two alist to one and return it"
  (should (equal (allmenu-merge-alists '(("Types" ("foo" . "bar")))
                                            '(("Variables" ("bar" . "baz"))))
                 '(("Variables" ("bar" . "baz")) ("Types" ("foo" . "bar"))))))

;; allmenu--make-index-alist return an alist with markers, these markers
;; can not be writen manually to compare them with a hand-made alist therefore
;; all cdrs are swapped for the word "buffer"

(defun change-index-cdr-to-string (alist)
  "Modify any index alist to make each index element a valid
string representation, in this way an valid index alist can be
compared to a hand-made index alist containing only strings"
  (mapcar (lambda (item)
            (cond ((imenu--subalist-p item)
                   (change-index-cdr-to-string item))
                  ((listp item)
                   (cons (substring-no-properties (car item)) "buffer"))
                  (t item)))
          alist))

(defun make-index-alist-callback ()
  "Test allmenu-choose-buffer-index. It should return an unify alist"
  (let ((current-allmenu-alist allmenu--index-alist) (tmp-alist nil))
    (unwind-protect
        (progn
          (setq allmenu--index-alist nil)
          (allmenu--make-index-alist)
          (setq tmp-alist (change-index-cdr-to-string allmenu--index-alist))
          (should (equal tmp-alist
                         '(("*Rescan*" . "buffer")
                           ("Includes"
                            ("stdio.h<>" . "buffer"))
                           ("Functions"
                            ("greeting()" . "buffer")
                            ("main()" . "buffer")
                            ("greeting()" . "buffer"))
                           ("Variables"
                            ("greeting" . "buffer")
                            ("name" . "buffer"))
                           ("test-enviroment" . "buffer")
                           ("buffer-list-callback" . "buffer")
                           ("test-allmenu--buffer-list" . "buffer")
                           ("test-allmenu-merge-alist" . "buffer")
                           ("change-index-cdr-to-string" . "buffer")
                           ("make-index-alist-callback" . "buffer")
                           ("test-allmenu--make-index-alist" . "buffer")
                           ("choose-buffer-index-callback" . "buffer")
                           ("test-allmenu-choose-buffer-index" . "buffer")
                           ("test-allmenu--generic-alist-p" . "buffer")
                           ("test-allmenu--add-marker-to-index-alist" . "buffer")
                           ("message-the-greeting" . "buffer")
                           ("Main" . "buffer")
                           ("Main#greeting" . "buffer")))))
      (setq allmenu--index-alist current-allmenu-alist))))

(ert-deftest test-allmenu--make-index-alist ()
    (test-enviroment 'make-index-alist-callback))

;; This test prompt the user for a index item, this item should be "greeting()" under
;; Funcionts subalist
(defun choose-buffer-index-callback ()
  "Test allmenu-choose-buffer index. It should prompt to the user
what index wants and return it"
  (let ((current-allmenu-alist allmenu--index-alist))
    (unwind-protect
        (progn
          (setq allmenu--index-alist nil)
          (with-current-buffer (current-buffer)
            (should (string= (car (allmenu-choose-buffer-index)) "greeting()"))))
      (setq allmenu--index-alist current-allmenu-alist))))

(ert-deftest test-allmenu-choose-buffer-index ()
  (test-enviroment 'choose-buffer-index-callback))

(ert-deftest test-allmenu--generic-alist-p ()
  "Test `allmenu--generic-alist-p'. It should return t if an index alist
has markers (format used by `imenu-generic-expression'"
  (let ((good-list `(("foo" . ,(make-marker)) ("bar" . ,(make-marker)))) ;; generic alist
        (bad-list '(("foo" . 1) ("bar" . 3)))) ;; non-generic alist
    (should (eq 't (allmenu--generic-alist-p good-list)))
    (should (eq 'nil (allmenu--generic-alist-p bad-list)))))

(ert-deftest test-allmenu--add-marker-to-index-alist ()
  "Test `allmenu--add-marker-to-index-alist'. It should return an alist with markers as cdr
of each index alist"
  (let ((foo (make-marker)) (bar (make-marker)))
    (set-marker foo 5)
    (set-marker bar 10)
    (should (equal `(("foo" . ,foo) ("bar" . ,bar))
                   (allmenu--add-marker-to-index-alist '(("foo" . 5) ("bar" . 10)))))))
