#include <stdio.h>

void greeting(char*);

int main(void){
  char *name = "Joe";
  greeting(name);
}

void greeting(char *name) {
  printf("Hi %s, how are you?", name);
}
