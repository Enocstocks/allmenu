(defvar greeting "Hi %s, how are you?"
  "Simple greeting")

(defvar name "Joe"
  "Any name")

(defun message-the-greeting nil
  "Print `greeting' with `name'"
  ()
  (message greeting name))

(message-the-greeting)
