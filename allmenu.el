;;; allmenu.el ---  Buffer menu generated with all the current buffers -*- lexical-binding: t -*-

;; Copyright (C) 2020 Bryan Hernández

;; Author: Bryan Hernández <masterenoc@tutamail.com>
;; Keywords: tools convenience

;;; Commentary

;; Overview:
;; This packages add an extra functionality to imenu, add a buffer menu
;; whose indexes are retrieved from all the current buffers. It uses some
;; imenu functions and uses the same format for indexes alist.

;; How it works:
;; The core functionality is show a buffer menu to the user, this buffer menu
;; contains all indexes found in all the current buffers. Buffers are traversed
;; and only the ones with qualified names are used by allmenu, then, it traverses
;; all qualified buffers and makes an alist for each buffer and merges those alist.

;;; Code:

(require 'imenu)

;;;###autoload
(defvar allmenu-searched-buffers nil
  "Contains buffers qualified to find indexes in them, this means buffers who did not matched
 with `allmenu-not-generate-alist-buffers'
This variables is set with `allmenu--buffers-list'")

;;;###autoload
(defvar allmenu-not-generate-alist-buffers '("\*.*\*")
  "List containing names or regexs of buffers that should not be a qualified buffer for indexes,
this variables is used to test all buffers against `allmenu--buffers-list' and retrieve
only qualified buffers or can be used to avoid buffers chosed by the user")

(defvar allmenu--index-alist nil
  "Contains an alist with all indexes of all qualified buffers, this variables is set by
`allmenu--make-index-alist'")

(defun allmenu--buffers-list ()
    "Set `allmenu-not-generate-alist-buffers' with a list of all the buffers that can contain an item
suitable for an index, this is done traversing all the currents buffers and find if any buffer name matches
`allmenu-not-generate-alist-buffers'"
       (setq allmenu-searched-buffers (mapcar 'buffer-name (buffer-list)))
       (mapc (lambda (buff)
               (mapc (lambda (regex)
                       (if (string-match-p regex buff)
                           (setq allmenu-searched-buffers (delete buff allmenu-searched-buffers))))
                     allmenu-not-generate-alist-buffers))
             allmenu-searched-buffers)
       allmenu-searched-buffers)

;;;###autoload
(defun allmenu-merge-alists (alist1 alist2)
  "Adds alist2 indexes to alist1, this keeps an unify alist with the format needed by `imenu' to work properly"
  (let ((matched nil) item)
    (while alist2
      (setq item (pop alist2))
      (setq matched nil)
      (cond ((imenu--subalist-p item)
             (mapc (lambda (alist1-item)
                     (if (and (imenu--subalist-p alist1-item)
                              (string= (car alist1-item) (car item)))
                         (progn (nconc (cdr alist1-item) (cdr item))
                                (setq matched t))))
                   alist1)
             (if (not matched)
                 (push item alist1)))
            (item
             (setq alist1 (append alist1 (list item))))))
    alist1))

(defun allmenu--generic-alist-p (alist)
  "Test if alist is an index alist obtained with `imenu-generic-expression'"
  (if (imenu--subalist-p (car alist))
      (allmenu--generic-alist-p (cdar alist))
    (or (markerp (cdar alist))
        (overlayp (cdar alist)))))

(defun allmenu--add-marker-to-index-alist (alist)
  "Add markers instead of positions to indexes alist (non-generic) created with `imenu-create-index-function'.

Modes that use its own function to create an index alist tends to return just a position instead
of marker as the cdr on an index. Markers will use the current buffer and the previous positioned
indicated by the index."
    (mapcar
   (lambda (index)
     (cond ((imenu--subalist-p index)
            (allmenu--add-marker-to-index-alist index))
           ((listp index)
            (let ((pos (cdr index)) (marker (make-marker)))
              (cons (car index) (set-marker marker pos))))
           (t index)))
   alist))

(defun allmenu--make-index-alist ()
  "Traverses all buffers in `allmenu--buffers-list' and makes an index alist all the indexes founds in
those buffers"
  (if allmenu--index-alist allmenu--index-alist
    (allmenu--buffers-list)
    (let ((curr-pos (point-marker)))
      (unwind-protect
          (with-current-buffer (current-buffer)
            (mapc (lambda (buff)
                    (set-buffer buff)
                    (setq allmenu--index-alist
                          (allmenu-merge-alists allmenu--index-alist
                                                (progn
                                                  (imenu--make-index-alist)
                                                  (if (allmenu--generic-alist-p imenu--index-alist)
                                                      imenu--index-alist
                                                    (allmenu--add-marker-to-index-alist imenu--index-alist))))))
                  allmenu-searched-buffers))
        (goto-char (marker-position curr-pos))))
    (when (eq allmenu--index-alist nil)
      (imenu-unavailable-error "No item suitable for an index in all buffers"))
    (setq allmenu--index-alist (cons imenu--rescan-item allmenu--index-alist))))

(defun allmenu-choose-buffer-index ()
  "Returns an index of an alist after prompt to the user which index wants.
If `imenu--rescan-item' is chosen or `allmenu--index-alist' is empty
and a new alist is made "
  (let ((chosen-index t))
    (while (eq chosen-index t)
      (if allmenu--index-alist
          (setq chosen-index (imenu--completion-buffer allmenu--index-alist))
        (allmenu--make-index-alist))
      (when (equal chosen-index imenu--rescan-item)
        (setq allmenu--index-alist nil)
        (allmenu--make-index-alist)
        (setq chosen-index t)))
    chosen-index))

;;;###autoload
(defun allmenu (index-item)
  "Jump to the position in the buffer using a buffer menu generated with
`allmenu-choose-buffer-index'"
  (interactive (list (allmenu-choose-buffer-index)))
  (let ((target-pos (cdr index-item)))
    (cond ((markerp target-pos)
           (switch-to-buffer (marker-buffer target-pos))
           (funcall 'imenu-default-goto-function "_" (marker-position target-pos)))
          ((overlayp target-pos)
           (switch-to-buffer (overlay-buffer target-pos))
           (funcall 'imenu-default-goto-function "_" (overlay-start target-pos))))))

(provide 'allmenu)

;;; allmenu.el ends here
